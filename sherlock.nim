import os
import std/httpclient
import std/json
import std/strutils
import std/threadpool

type
    Site = object
        name: string
        url: string
        account_existence_code: string
        account_existence_string: string
        account_missing_code: string
        account_missing_string: string

proc checkSite(site: Site, username: string) =
    let complete_url = site.url.replace("%", username)
    let client = newHttpClient()
    let response = client.get(complete_url)
    if response.code == Http200:
        echo "[+] " & site.name & ": " & complete_url 

let username = paramStr(1)
let sites = parseFile("sites.json")

for s in sites:
    let site = to(s, Site)
    spawn checkSite(site, username)
sync()
