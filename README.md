# nim-sherlock

A quick and dirty write of [sherlock](https://github.com/sherlock-project/sherlock) in Nim.

Config data copied from [WhatsMyName](https://github.com/WebBreacher/WhatsMyName)

Uses `sites.json` for configuration.

Go a more complete rewrite, check out my Go version: [go-sherlock](https://gitlab.com/drskelebones/go-sherlock)
